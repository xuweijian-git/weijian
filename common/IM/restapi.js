//用户数据示例
let users = [
    {
        "uuid": "08c0a6ec-a42b-47b2-bb1e-15e0f5f9a19a",
        "name": "1",
        "password": "123",
        "avatar": '../../static/images/chat/1.jpg'
    },
    {
        "uuid": "3bb179af-bcc5-4fe0-9dac-c05688484649",
        "name": "2",
        "password": "123",
        "avatar": '../../static/images/chat/2.jpg'
    },
    {
        "uuid": "fdee46b0-4b01-4590-bdba-6586d7617f95",
        "name": "3",
        "password": "123",
        "avatar": '../../static/images/chat/3.jpg'
    },
    {
        "uuid": "33c3693b-dbb0-4bc9-99c6-fa77b9eb763f",
        "name": "4",
        "password": "123",
        "avatar": '../../static/images/chat/4.jpg'
    },{
	    "id": "5",
	    "name": "5",
	    "password": "123",
	    "avatar": '../../static/images/chat/5.jpg'
	},
	{
	    "id": "6",
	    "name": "6",
	    "password": "123",
	    "avatar": '../../static/images/chat/6.jpg'
	},
	{
	    "id": "7",
	    "name": "7",
	    "password": "123",
	    "avatar": '../../static/images/chat/7.jpg'
	},
	{
	    "id": "8",
	    "name": "8",
	    "password": "123",
	    "avatar": '../../static/images/chat/8.jpg'
	},
	{
	    "id": "9",
	    "name": "9",
	    "password": "123",
	    "avatar": '../../static/images/chat/9.jpg'
	},
	{
	    "id": "10",
	    "name": "10",
	    "password": "123",
	    "avatar": '../../static/images/chat/10.jpg'
	},
	{
	    "id": "11",
	    "name": "11",
	    "password": "123",
	    "avatar": '../../static/images/chat/11.jpg'
	},
	{
	    "id": "12",
	    "name": "12",
	    "password": "123",
	    "avatar": '../../static/images/chat/12.jpg'
	}
];

//群数据示例
let groups = [
    {
        "uuid": "group-a42b-47b2-bb1e-15e0f5f9a19a",
        "name": "群1",
        "userList": ['08c0a6ec-a42b-47b2-bb1e-15e0f5f9a19a', '3bb179af-bcc5-4fe0-9dac-c05688484649', 'fdee46b0-4b01-4590-bdba-6586d7617f95', '33c3693b-dbb0-4bc9-99c6-fa77b9eb763f']
    },
    {
        "uuid": "group-4b01-4590-bdba-6586d7617f95",
        "name": "群2",
        "userList": ['08c0a6ec-a42b-47b2-bb1e-15e0f5f9a19a', 'fdee46b0-4b01-4590-bdba-6586d7617f95', '33c3693b-dbb0-4bc9-99c6-fa77b9eb763f']
    },
    {
        "uuid": "group-dbb0-4bc9-99c6-fa77b9eb763f",
        "name": "群3",
        "userList": ['08c0a6ec-a42b-47b2-bb1e-15e0f5f9a19a', '3bb179af-bcc5-4fe0-9dac-c05688484649']
    }
];


function RestApi() {

}

RestApi.prototype.findFriends = function (user) {
    var friendList = users.filter(v => v.uuid != user.uuid);
    return friendList;
}

RestApi.prototype.findGroups = function (user) {
    var groupList = groups.filter(v => v.userList.find(id => id == user.uuid));
    return groupList;
}

RestApi.prototype.findUser = function (username, password) {
    var user = users.find(user => (user.name == username && user.password == password))
    return user;
}

RestApi.prototype.findGroupMembers = function (groupId) {
    let members = [];
    let group = groups.find(v => v.uuid == groupId);
    users.map(user => {
        if (group.userList.find(v => v == user.uuid)) {
            members.push(user)
        }
    });
    return members;
}

export default new RestApi();