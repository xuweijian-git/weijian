const commonFun = {
	/* 封装原生toast轻量提示框 */
	msg: (title, duration = 1500, mask = false, icon = 'none') => {
		if (Boolean(title) === false) {
			return;
		}
		uni.showToast({
			title,
			duration,
			mask,
			icon
		});
	},
	/* 封装获取节点元素尺寸 */
	getElSize: (classname) => { //得到元素的size
		return new Promise((res, rej) => {
			uni.createSelectorQuery().select('.' + classname).fields({
				size: true,
				scrollOffset: true
			}, (data) => {
				res(data);
			}).exec();
		});
	}
}
export default commonFun
