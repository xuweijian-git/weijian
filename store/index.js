import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
//如果state里面的其中一个键值发生改变那就存储，没改变就不存储
var state =  uni.getStorageSync('state')?uni.getStorageSync('state'):{

}
const store = new Vuex.Store({
    state,
    mutations: {

    }
})

export default store